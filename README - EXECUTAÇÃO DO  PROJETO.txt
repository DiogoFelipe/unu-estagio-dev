--Execução do Projeto--

Todos os arquivos devem ser colocados em um servidor.
O arquivo sistemaacademico.sql deve ser importado para o banco de dados e será criado uma nova base de dados com o nome sistemaacademico caso não exista, juntamente com as tabelas.
O arquivo conexao.php deve ser editado para colocar os parametros do banco de dados do servidor do tester.

*Será necessário cadastrar pelo menos um aluno para fazer os testes.
O botão 'Novo Aluno' cadastra um aluno.
Na página inicial há o botão 'buscar' que apresentará por paginação 50 alunos por ordem de ID. O campo também possibilita a apresentação de algum aluno específico basta preenchelo e clicar no botão 'buscar'.
Para detalhar as informações do aluno é necessário pesquisá-lo e clicar em cima do nome.
O botão de 'editar' e 'excluir' aparece quando se pesquisa o aluno.

Para simplificar coloquei o projeto online: https://scintillant-torpedo.000webhostapp.com
Também está no GitHub: https://github.com/diogofelipeladorucki/Sistema-Academico